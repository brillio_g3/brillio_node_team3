const express=require('express');

const inventoryService=require('../services/inventory')

const inventoryRouter=express.Router();

    
inventoryRouter.get('/',inventoryService.getInventory)

inventoryRouter.post('/',inventoryService.createInventory)

inventoryRouter.put('/:productId',inventoryService.updateInventory)

inventoryRouter.delete('/:productId',inventoryService.deleteInventory)

module.exports = inventoryRouter
