const express=require('express');

const siteService=require('../services/site')

const siteRouter=express.Router();

    
siteRouter.get('/',siteService.getSite)

siteRouter.post('/',siteService.createSite)

siteRouter.put('/:ticketNumber',siteService.updateSite)

siteRouter.delete('/:ticketNumber',siteService.deleteSite)

module.exports = siteRouter
