const connection = require('../../database')
// const { v4: uuid4 } = require('uuid')
const notification = require('./notification')
// const updateNotification = require('./notification')

function generateTicketNumber() {
    const prefix = 'TIC';
    const uniqueId = Math.floor(1000 + Math.random() * 9000);
    return `${prefix}${uniqueId}`
}

exports.getSite = async (req, res) => {
    try {
        const categoryOfEquipment = req.query.categoryOfEquipment
        const location = req.query.location
        const description = req.query.description
        const ownerMail = req.query.ownerMail
        const userMail = req.query.userMail
        const name = req.query.name
        console.log(req.query)
        let sql = 'SELECT * FROM telecom.site WHERE 1=1 ';
        let queryParams = []
        if (categoryOfEquipment) {
            sql += ' AND categoryOfEquipment =?'
            queryParams.push(categoryOfEquipment)
        }
        if (location) {
            sql += ' AND location =?'
            queryParams.push(location)
        }
        if (description) {
            sql += ' AND description =?'
            queryParams.push(description)
        }
        if (ownerMail) {
            sql += ' AND ownerMail =?'
            queryParams.push(ownerMail)
        }
        if (userMail) {
            sql += ' AND userMail =?'
            queryParams.push(userMail)
        }
        if (name) {
            sql += ' AND name =?'
            queryParams.push(name)
        }
        console.log(sql)
        connection.query(sql, queryParams, function (err, result) {
            if (err) {
                console.log(err);
                res.status(500).json({ error: "Failed to get data" });
                return;
            };
            console.log(result);
            if (result && result.length != 0) {
                res.status(200).send(result);
            } else {
                return res.status(400).send('Site Details not found')
            }
        });
    } catch (error) {
        res.status(404).send(error)
    }
}

exports.createSite = async (req, res) => {
    try {
        if(req.body){
            req.body.ticketNumber = generateTicketNumber()
        const { ticketNumber, name, categoryOfEquipment, location, description, userMail, ownerMail } = req.body;

        let sql = "INSERT INTO telecom.site (ticketNumber, name, categoryOfEquipment, location, description, usermail, ownerMail) VALUES (?,?,?,?,?,?,?)";
        connection.query(sql, [ticketNumber, name, categoryOfEquipment, location, description, userMail, ownerMail], async function (err, result) {
            if (!err) {
                console.log('Site added successfully', result);
                const mailRes = notification.createNotification(req.body)
                if (mailRes) {
                    res.json({
                        status: 200,
                        message: "Site Request Created and Mail sent."
                    })
                }
            } else {
                console.log(err);
                res.status(500).json({ error: "Failed to create data" });
                return;
            }
        })
        } else {
            res.status(500).json({error:'Request body not found'})
        }
        
    } catch (error) {
        res.status(404).send(error)
    }
}

exports.updateSite = async (req, res) => {
    try {
        if(req.body){
            const { ticketNumber, location } = req.body;
        let sql = "UPDATE telecom.site SET location = ? WHERE ticketNumber = ?";
        connection.query(sql, [location, ticketNumber], async function (err, result) {
            if (!err) {
                console.log(result);
                let siteDetail = 'SELECT ownerMail, userMail FROM telecom.site WHERE ticketNumber = ?'
                connection.query(siteDetail, [ticketNumber], async function (err, result) {
                    if (!err) {
                        const siteDetail = result[0]
                        await notification.updateNotification(req.body, siteDetail)
                        res.json({
                            status: 200,
                            message: "Site Request Updated and Mail sent."
                        })
                    } else {
                        throw err
                    }

                })
            } else {
                console.log(err);
                res.status(500).json({ error: "Failed to update data" });
                return;
            }
        });
        }else {
            res.status(500).json({error:'Request body not found'})

        }
    } catch (error) {
        res.status(404).send(error)
    }
}

exports.deleteSite = async (req, res) => {
    try {
        if(req.params.ticketNumber){
            const ticketNumber = req.params.ticketNumber;
        let sql = "SELECT usermail, ownerMail FROM telecom.site WHERE ticketNumber = ?"
        connection.query(sql, [ticketNumber], async function (err, result) {
            if (!err) {
                const toAddress = result[0]
                console.log(toAddress)
                let siteDetail = "DELETE FROM telecom.site WHERE ticketNumber = ?";
                connection.query(siteDetail, [ticketNumber], async function (err, result) {
                    if (!err) {
                        console.log('record deleted')
                        await notification.deleteNotification(req.params, toAddress)
                        res.json({
                            status: 200,
                            message: "Site Request Updated and Mail sent."
                        })

                    } else {
                        throw err
                    }

                })
            } else {
                console.log(err);
                res.status(500).json({ error: "Failed to delete data" });
                return;
            }
        });
        } else {
            res.status(500).json({error:'Ticket Number not found'})

        }
        
    } catch (error) {
        res.status(404).send(error)
    }
}
