const connection = require('../../database')
// const { v4: uuid4 } = require('uuid')
const sendEmail = require('./utilities')

function generateProductId() {
    const prefix = 'PRD';
    const uniqueId = Math.floor(1000 + Math.random() * 9000);
    return `${prefix}${uniqueId}`
}
const getFromAddress = async (productId) => {
    let sql = 'SELECT ownerMail FROM telecom.inventory WHERE productId = ?'
    const fromAddress = connection.query(sql, [productId], function (err, result) {
        if (!err) {
            return fromAddress
        } else {
            throw err
        }
    })
}
exports.getInventory = async (req, res) => {
    try {
        const categoryOfEquipment = req.query.categoryOfEquipment
        const location = req.query.location
        const availableQty = req.query.availableQty
        const ownerMail = req.query.ownerMail
        console.log(req.query)
        let sql = 'SELECT * FROM telecom.inventory WHERE true '; // not a filter
        let queryParams = []
        if (categoryOfEquipment) {
            sql += ' AND categoryOfEquipment =?'
            queryParams.push(categoryOfEquipment)
        }
        if (location) {
            sql += ' AND location =?'
            queryParams.push(location)
        }
        if (availableQty) {
            sql += ' AND availableQty =?'
            queryParams.push(availableQty)
        }
        if (ownerMail) {
            sql += ' AND ownerMail =?'
            queryParams.push(ownerMail)
        }
        console.log(sql)
        connection.query(sql, queryParams, function (err, result) {
            if (err) {
                console.log(err);
                res.status(500).json({ error: "Failed to get data" });
                return;
            };
            console.log(result);
            if (result && result.length != 0) {
                res.status(200).send(result);
            } else {
                return res.status(400).send('Inventory Details not found')
            }
        });
    } catch (error) {
        res.status(404).send(error)
    }

}

exports.createInventory = async (req, res) => {
    try {
        if (req.body) {
            req.body.productId = generateProductId()
            // req.body.availableQty = req.body.totalQty
            const { productId, name, categoryOfEquipment, totalQty, availableQty, location, description, ownerMail } = req.body;

            let sql = "INSERT INTO telecom.inventory (productId, name, categoryOfEquipment, totalQty, availableQty, location, description, ownerMail) VALUES (?,?,?,?,?,?,?,?)";
            connection.query(sql, [productId, name, categoryOfEquipment, totalQty, availableQty, location, description, ownerMail], async function (err, result) {
                if (!err) {
                    console.log('inventory added successfully', result);
                    const mailOptions = {
                        from: 'youremail.com', // sender address
                        to: ownerMail, // list of receivers
                        subject: 'Inventory Added',
                        template: 'addInventoryEmail', // the name of the template file i.e email.handlebars
                        context: {
                            productId: productId,
                            name: name,
                            categoryOfEquipment: categoryOfEquipment,
                            totalQty: totalQty,
                            availableQty: availableQty,
                            location: location,
                            description: description,
                            ownerMail: ownerMail
                        }
                    }
                    await sendEmail(mailOptions)
                    res.json({
                        status: 200,
                        message: "Inventory added and Mail sent."
                    });

                } else {
                    console.log(err);
                    res.status(500).json({ error: "Failed to create data" });
                    return;
                }
            })
        } else {
            res.status(404).send('request body is not present')
        }
    } catch (error) {
        res.status(404).send(error)
    }
}

exports.updateInventory = async (req, res) => {
    try {
        if (req.body.productId && req.body.availableQty) {
            const { productId, availableQty } = req.body;
            let sql = "UPDATE telecom.inventory SET availableQty = ? WHERE productId = ?";
            connection.query(sql, [availableQty, productId], async function (err, result) {
                if (!err) {
                    console.log(result);
                    let inventoryDetail = 'SELECT ownerMail FROM telecom.inventory WHERE productId = ?'
                    connection.query(inventoryDetail, [productId], async function (err, result) {
                        if (!err) {
                            const toAddress = result[0]
                            console.log(toAddress)
                            if (toAddress) {
                                const mailOptions = {
                                    from: 'youremail.com', // sender address
                                    to: toAddress.ownerMail, // list of receivers
                                    subject: 'Inventory Updated',
                                    template: 'updateInventoryEmail', // the name of the template file i.e email.handlebars
                                    context: {
                                        productId: productId,
                                        availableQty: availableQty
                                    }
                                }

                                await sendEmail(mailOptions)
                                res.json({
                                    status: 200,
                                    message: "Inventory updated and Mail sent."
                                })
                            }
                        } else {
                            throw err
                        }
                    })

                } else {
                    console.log(err);
                    res.status(500).json({ error: "Failed to update data" });
                    return;
                }
            });
        } else {
            res.status(404).send('productId or availableQty not present')
        }


    } catch (error) {
        res.status(404).send(error)
    }
}

exports.deleteInventory = async (req, res) => {
    try {
        if (req.params.productId) {
            const productId = req.params.productId;
            let sql = "SELECT ownerMail FROM telecom.inventory WHERE productId = ?"

            connection.query(sql, [productId], async function (err, result) {
                if (!err) {
                    const toAddress = result[0]
                    console.log(toAddress)
                    let inventoryDetail = 'DELETE FROM inventory WHERE productId = ?'
                    connection.query(inventoryDetail, [productId], async function (err, result) {
                        if(!err){
                            console.log('deleted record')
                            if(toAddress){
                                const mailOptions = {
                                    from: 'youremail.com', // sender address
                                    to: toAddress.ownerMail, // list of receivers
                                    subject: 'Inventory Deleted',
                                    template: 'deleteInventoryEmail', // the name of the template file i.e email.handlebars
                                    context: {
                                        productId: productId
                                    }
                                }
                                await sendEmail(mailOptions)
                                res.json({
                                    status: 200,
                                    message: "Inventory deleted and Mail sent."
                                })
                            }
                        } else {
                            throw err
                        }
                    })
                } else {
                    console.log(err);
                    res.status(500).json({ error: "Failed to delete data" });
                    return;
                }
            });
        } else {
            res.status(404).send('request param productId not present')
        }

    } catch (error) {
        res.status(404).send(error)
    }
}