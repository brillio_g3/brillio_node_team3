const hbs = require('nodemailer-express-handlebars')
const nodemailer = require('nodemailer')
const path = require('path')

const sendEmail = async (mailOptions) => {
    // initialize nodemailer
    const transporter = nodemailer.createTransport(
        {
            service: 'gmail',
            auth: {
                user: 'youremail.com',
                pass: 'maxd dbst rjkm nkfw'
            }
        }
    );

    // point to the template folder
    const handlebarOptions = {
        viewEngine: {
            partialsDir: path.resolve('./views/'),
            defaultLayout: false,
        },
        viewPath: path.resolve('./views/'),
    };

    // use a template file with nodemailer
    transporter.use('compile', hbs(handlebarOptions))



    // trigger the sending of the E-mail
    const mailRes = transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            return (error)
        } else {
            console.log('Message sent: ' + info.response);
            return info.response
        }
    });
}
module.exports = sendEmail