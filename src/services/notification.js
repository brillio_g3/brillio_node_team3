const sendMail = require('./utilities')
const connection = require('../../database')
// const imap = require('imap-simple')
// const {simpleParser} = require('mailParser')



exports.createNotification = async (details) => {
    const ticketNumber = details.ticketNumber
    const ticketDetails = {
        name: details.name,
        categoryOfEquipment: details.categoryOfEquipment,
        location: details.location,
        description: details.description,
        userMail: details.userMail,
        ownerMail: details.ownerMail,
    }

    const changeFlag = 'pending'
    let sql = "INSERT INTO telecom.notification (ticketNumber, ticketDetails, changeFlag) VALUES (?,?,?)";
    connection.query(sql, [ticketNumber, JSON.stringify(ticketDetails), changeFlag], async function (err, result) {
        if (!err) {
            console.log('notification saved successfully', result);
            const toAddress = `${details.ownerMail},${details.userMail}`
            // send mail
            const mailOptions = {
                from: 'youremail.com', // sender address
                to: toAddress, // list of receivers
                subject: 'Site Request Created',
                template: 'addSiteEmail', // the name of the template file i.e email.handlebars
                context: {
                    ticketNumber: ticketNumber,
                    ticketDetails: ticketDetails,
                    status: changeFlag
                }
            };
            const mailRes = await sendMail(mailOptions);
            if (mailRes) {
                return mailRes
            }
        } else {
            console.log(err);
            
            return err;
        }
    })
}

exports.updateNotification = async (details, address) => {
    const ticketNumber = details.ticketNumber
    const location = details.location
    let sql = "UPDATE telecom.notification SET ticketDetails = JSON_SET(ticketDetails, '$.location', ?) WHERE ticketNumber = ?";
    connection.query(sql, [location, ticketNumber], async function (err, result) {
        if (!err) {
            console.log('notification updated successfully', result);
            const toAddress = `${address.ownerMail},${address.userMail}`
            // send mail
            const mailOptions = {
                from: 'youremail.com', // sender address
                to: toAddress, // list of receivers
                subject: 'Site Request Updated',
                template: 'updateSiteEmail', // the name of the template file i.e email.handlebars
                context: {
                    ticketNumber: ticketNumber,
                    location: location                }
            };
            const mailRes = await sendMail(mailOptions);
                return mailRes
        } else {
            console.log(err);
            
            return err;
        }
    })
}

exports.deleteNotification = async (details, address) => {
    const ticketNumber = details.ticketNumber
    let sql = "DELETE FROM telecom.notification WHERE ticketNumber = ?";
    connection.query(sql, [ticketNumber], async function (err, result) {
        if (!err) {
            console.log('notification deleted successfully', result);
            const toAddress = `${address.ownerMail},${address.userMail}`
            // send mail
            const mailOptions = {
                from: 'youremail.com', // sender address
                to: toAddress, // list of receivers
                subject: 'Site Request Deleted',
                template: 'deleteSiteEmail', // the name of the template file i.e email.handlebars
                context: {
                    ticketNumber: ticketNumber
                }
            };
            const mailRes = await sendMail(mailOptions);
            return mailRes
        } else {
            console.log(err);
           
            return err;
        }
    })
}

