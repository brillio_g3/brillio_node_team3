const express = require("express");
const app = express();
const cors = require('cors');
const siteRouter = require("./src/routes/site");
const inventoryRouter = require("./src/routes/inventory")
// const connection = require('./database')
/*middleware*/
app.use(cors());
app.use(express.json()) // to read req.body otherwise it is undefined
app.use(express.urlencoded({
    extended: true
}))
var corsOptions = {

    origin: true,

    methods: ["POST", "GET", "PUT"],

    credentials: true,

    maxAge: 3600

}
app.use(cors(corsOptions));
var requestDate = function (req, res, next) {
  req.requestDate = new Date().toLocaleString('en-US', { timeZone: 'Asia/Kolkata' });
  next();
};
app.use('/inventory', inventoryRouter)
app.use('/site',siteRouter)

app.listen(3000, function () {
  console.log("Listening on server at port 3000");
});
module.exports = app